package handsome.code.monkeys.optaplanner;

import org.optaplanner.core.api.score.buildin.hardsoftlong.HardSoftLongScore;
import org.optaplanner.core.api.score.stream.Constraint;
import org.optaplanner.core.api.score.stream.ConstraintCollectors;
import org.optaplanner.core.api.score.stream.ConstraintFactory;
import org.optaplanner.core.api.score.stream.ConstraintProvider;
import org.optaplanner.core.api.score.stream.Joiners;

import handsome.code.monkeys.data.Antenna;
import handsome.code.monkeys.data.Building;
import lombok.experimental.UtilityClass;

public class ProblemConstraintProvider implements ConstraintProvider {
	@UtilityClass
	private class Utility {
		int dist(final Building b, final Antenna a) {
			return Math.abs(b.getX() - a.getX()) + Math.abs(b.getY() - a.getY());
		}

		boolean r(final Building b, final Antenna a) {
			return a.getPoint() == null ? false : dist(b, a) <= a.getR();
		}

		int s(final Building b, final Antenna a) {
			return b.getC() * a.getC() - b.getL() * dist(b, a);
		}
	}

	private Constraint antennaConflict(final ConstraintFactory constraintFactory) {
		return constraintFactory.forEachUniquePair(Antenna.class, Joiners.equal(Antenna::getPoint))
				.penalize("Antenna conflict", HardSoftLongScore.ONE_HARD);
	}

	private Constraint buildingScore(final ConstraintFactory constraintFactory) {
		return constraintFactory.forEach(Building.class).join(Antenna.class, Joiners.filtering(Utility::r))
				.groupBy((building, antenna) -> building, ConstraintCollectors.max(Utility::s))
				.reward("Building score", HardSoftLongScore.ONE_SOFT, (building, c) -> Math.max(0, c));
	}

	@Override
	public Constraint[] defineConstraints(ConstraintFactory constraintFactory) {
		return new Constraint[] { antennaConflict(constraintFactory), buildingScore(constraintFactory),
				reward(constraintFactory) };
	}

	private Constraint reward(final ConstraintFactory constraintFactory) {
		return constraintFactory.forEach(Integer.class).reward("Reward", HardSoftLongScore.ONE_SOFT, Integer::intValue);
	}
}
